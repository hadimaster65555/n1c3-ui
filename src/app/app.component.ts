import { Component } from '@angular/core';

import { PotService } from './service/pot.service';

import {Router} from "@angular/router";
import { InformasiService } from './service/informasi.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [PotService, InformasiService]
})
export class AppComponent{
  title = 'app';

  constructor(){

  }

}
