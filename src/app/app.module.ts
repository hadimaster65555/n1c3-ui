import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SpiFirstComponent } from './spi-first/spi-first.component';
import { RouterModule, Routes } from '@angular/router';
import { PotComponent } from './pot/pot.component';
import { WaitingComponent } from './waiting/waiting.component';
import { InformasiService } from './service/informasi.service';
import { PotService } from './service/pot.service';
import { BreakdownService } from './service/breakdown.service';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { RestsourceService } from './service/restsource.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PerformanceComponent } from './dashboard/performance/performance.component';
import { GanttComponent } from './dashboard/gantt/gantt.component';
import { AchievementComponent } from './dashboard/achievement/achievement.component';
import { EfisiensiComponent } from './dashboard/efisiensi/efisiensi.component';
import { SearchpipemoduleModule } from './pipe/searchpipemodule.module';
import { TokenstorageService } from './service/tokenstorage.service';
import { AuthenticationService } from './service/authentication.service';
import { HttpModule } from '@angular/http';

// import { ChartModule,HIGHCHARTS_MODULES } from 'angular-highcharts';
// import exporting from 'highcharts/modules/exporting.src.js';
// export function highchartsModules(){ 
//   return [exporting]; 
// }

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'spi-first', component: SpiFirstComponent },
  { path: 'pot', component: PotComponent },
  { path: 'waiting', component: WaitingComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'achievement', component: AchievementComponent },
  { path: 'efisiensi', component: EfisiensiComponent },
  { path: 'gantt', component: GanttComponent },
  { path: 'performance', component: PerformanceComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SpiFirstComponent,
    PotComponent,
    WaitingComponent,
    DashboardComponent,
    PerformanceComponent,
    GanttComponent,
    AchievementComponent,
    EfisiensiComponent
  ],
  imports: [
    SearchpipemoduleModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    ReactiveFormsModule,
    TypeaheadModule.forRoot(),
    RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  providers: [InformasiService, PotService, BreakdownService, RestsourceService,
    TokenstorageService, AuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
