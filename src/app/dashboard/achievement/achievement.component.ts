import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BreakdownService } from '../../service/breakdown.service';
import { PotService } from '../../service/pot.service';
import { InformasiService } from '../../service/informasi.service';
import { OFAchievement } from '../../model/OFAchievement';
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
  selector: 'app-achievement',
  templateUrl: './achievement.component.html',
  styleUrls: ['./achievement.component.css'],
  providers: [BreakdownService]
})
export class AchievementComponent implements OnInit, OnDestroy {


  ngOnDestroy() {
    this.stopTimer();
    this.sub.unsubscribe();
  }

  statusAchievement: OFAchievement = {individual: null, group: null};

  //Untuk Timer
  ticks = 0;
  minutesDisplay: number = 0;
  hoursDisplay: number = 0;
  secondsDisplay: number = 0;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private breakdownService: BreakdownService,
    private potService: PotService,
    private informasiService: InformasiService) { }

  ngOnInit() {
    this.startTimer();
    this.getAchievement();
  }

  getAchievement() {
    this.breakdownService.achievementOF().subscribe(
      status => {
        this.statusAchievement = status;
        console.log(this.statusAchievement)

        console.log("Individual Achievement : "+this.statusAchievement.individual+" Group Achievement : "+this.statusAchievement.group)

      },
      err => {
        console.log(err);
      }
    );

  }

  private stopTimer() {
    this.sub.unsubscribe();
    this.hoursDisplay = 0;
    this.minutesDisplay = 0;
    this.secondsDisplay = 0;
  }

  private startTimer() {
    this.hoursDisplay = 0;
    this.minutesDisplay = 0;
    this.secondsDisplay = 0;
    let timer = Observable.timer(1, 1000);
    this.sub = timer.subscribe(
      t => {
        this.ticks = t;

        this.secondsDisplay = this.getSeconds(this.ticks);
        this.minutesDisplay = this.getMinutes(this.ticks);
        this.hoursDisplay = this.getHours(this.ticks);
        // console.log(this.secondsDisplay)
        if (this.secondsDisplay % 10 == 0 && this.secondsDisplay != 0){
          this.getAchievement();
        }
      }
    );
  }



  //===============UNTUK TIMER=====================
  private getSeconds(ticks: number) {
    return this.pad(ticks % 60);
  }

  private getMinutes(ticks: number) {
    return this.pad((Math.floor(ticks / 60)) % 60);
  }

  private getHours(ticks: number) {
    return this.pad(Math.floor((ticks / 60) / 60));
  }

  private pad(digit: any) {
    return digit <= 9 ? '0' + digit : digit;
  }

}
