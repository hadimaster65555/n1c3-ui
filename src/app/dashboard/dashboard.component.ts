import { Component, OnInit, OnDestroy } from '@angular/core';

import { BreakdownService } from '../service/breakdown.service';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable, Subscription } from 'rxjs/Rx';
import { PotService } from '../service/pot.service';
import { InformasiService } from '../service/informasi.service';
import { ActionType } from '../model/ActionType';
import { Idle } from '../model/Idle';
import { StatusPWR } from '../model/StatusPWR';
import { UserOF } from '../model/UserOF';
import { RestsourceService } from '../service/restsource.service';
import { AuthenticationService } from '../service/authentication.service';
import { Userlogin } from '../model/userlogin';
import { Userofmodel } from '../model/userofmodel';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [BreakdownService,AuthenticationService,RestsourceService,InformasiService]
})
export class DashboardComponent implements OnInit, OnDestroy {

  private StatusIdleBreakdown: Idle[] = [];
  private StatusPWR: StatusPWR = { rm: null, pack: null, weigh: null };
  // Data for local storage
  data: any = [];
  // User Login Model
  userLogin: Userlogin = new Userlogin(null, null, null, null);
  //Variabel Untuk RM, PACK dan WEIGH
  statusRM: number;
  statusPACK: number;
  statusWEIGH: number;
  jumlahRM: number;
  totalWeigh: number;

  btnEndOf: String;
  widthRM: number;
  widthWeigh: number;


  // Informasi Login
  nik: String;
  namaOperator: String;
  Shift: number;
  Formula: String;
  Of: number;

  userOfModel: Userofmodel;

  //Untuk Timer
  ticks = 0;
  minutesDisplay: number = 0;
  hoursDisplay: number = 0;
  secondsDisplay: number = 0;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private breakdownService: BreakdownService,
    private potService: PotService,
    private informasi: InformasiService,
    private restSourceService: RestsourceService,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    // Check Credentials (To Check Who The Hell Are You)
    // this.authenticationService.isAuthenticated();
     // Are you bring something I need?
  this.authenticationService.checkCredentials();
    // checkCredentials();
   // Fetch Data From Local Storage
   var data = JSON.parse(localStorage.getItem('$2a$10$eKXxFtLgnQAoQYhtccIj5egtPHD7yFslrXeIDtMnyNxeMV.NzC51C'));
   var shiftOf = JSON.parse(localStorage.getItem('user-shift'));
   this.informasi.nama = atob (data['nama']);
   this.informasi.nik = atob (data['nik']);
   this.informasi.Shift = shiftOf['shift'];
   this.informasi.OF = shiftOf['ofNo'];
   this.informasi.Formula = shiftOf['formula'];
   this.informasi.OrderQuantity = shiftOf['oq'];
   this.informasi.RMQuantity = shiftOf['rm'];
   this.informasi.WB = shiftOf['wb'];
   console.log('nilai shift nya adalah ' + this.informasi.Shift);
    this.informasiLoad();
    this.potService.Status = 'Idle';
    this.startTimer();
    this.getStatusPWR();
    this.jumlahRM = this.informasi.RMQuantity;
    this.totalWeigh = this.informasi.OrderQuantity;
    console.log("OF DARI DASHBOARD "+this.informasi.OF);
    console.log("NIK DARI DASHBOARD "+this.informasi.nik);
    console.log("NAMA DARI DASHBOARD "+this.informasi.nama);
    console.log("WB DARI DASHBOARD "+this.informasi.WB);
  }

  ngOnDestroy() {
    this.stopTimer();
    this.sub.unsubscribe();
  }

  // BARU TAMBAHAN
  btnPot() {
    this.potService.Equipment = this.informasi.WB;
    this.potService.LoginName = this.informasi.nik;
    this.potService.Shift = this.informasi.Shift;
    this.potService.Tanggal = new Date();
    this.potService.Start = new Date();
    this.potService.Status = 'Manual';

    this.potService.statusPACK = this.statusPACK;
    this.potService.statusRM = this.statusRM;
    this.potService.statusWEIGH = this.statusWEIGH;
    this.potService.jumlahRM = this.jumlahRM;
    this.potService.totalWeigh = this.totalWeigh;
    this.potService.widthRM = this.widthRM;
    this.potService.widthWeigh = this.widthWeigh;

    this.stopTimer();
    this.sub.unsubscribe();
    this.hoursDisplay = 0;
    this.minutesDisplay = 0;
    this.secondsDisplay = 0;
  }

  logout() {
    // this.stopTimer();
    // this.sub.unsubscribe();
    // this.hoursDisplay = 0;
    // this.minutesDisplay = 0;
    // this.secondsDisplay = 0;
    this.data = JSON.parse(localStorage.getItem('$2a$10$eKXxFtLgnQAoQYhtccIj5egtPHD7yFslrXeIDtMnyNxeMV.NzC51C'));
    console.log(this.data);
    this.userLogin.nik = atob(this.data['nik']);
    this.userLogin.login = this.data['login'];
    this.userLogin.logout = Date.now();
    this.userLogin.shift = this.informasi.Shift;
    localStorage.removeItem('user-shift');
    this.restSourceService.postUserLogin(this.userLogin).subscribe((userLogin) => {
    console.log(userLogin);
    });
    this.stopTimer();
    this.sub.unsubscribe();
    this.hoursDisplay = 0;
    this.minutesDisplay = 0;
    this.secondsDisplay = 0;
    this.authenticationService.signout();
    // logout();
  }

  backFormInput() {
    this.sub.unsubscribe();
    this.hoursDisplay = 0;
    this.minutesDisplay = 0;
    this.secondsDisplay = 0;
  }

  informasiLoad() {
    if (this.informasi.nama == null) {
      this.namaOperator = 'Mattasir';
    } else {
      this.namaOperator = this.informasi.nama;
    }
    if (this.informasi.nik == null) {
      this.nik = 'J69511';
    } else {
      this.nik = this.informasi.nik;
    }
    if (this.informasi.Formula == null) {
      this.Formula = 'ZZ92502583';
    } else {
      this.Formula = this.informasi.Formula;
    }
    if (this.informasi.OF == null) {
      this.Of = 400000076099
    } else {
      this.Of = this.informasi.OF;
    }
    if (this.informasi.Shift == null) {
      this.Shift = 1;
    } else {
      this.Shift = this.informasi.Shift;
    }
  }

  openPOT() {
    this.potService.statusPACK = this.statusPACK;
    this.potService.statusRM = this.statusRM;
    this.potService.statusWEIGH = this.statusWEIGH;
    this.potService.jumlahRM = this.jumlahRM;
    this.potService.totalWeigh = this.totalWeigh;
    this.potService.widthRM = this.widthRM;
    this.potService.widthWeigh = this.widthWeigh;
    this.potService.Start = new Date();
    document.getElementById("openPot").click();
    this.sub.unsubscribe();
    this.hoursDisplay = 0;
    this.minutesDisplay = 0;
    this.secondsDisplay = 0;
  }

  private stopTimer() {
    this.sub.unsubscribe();
    this.hoursDisplay = 0;
    this.minutesDisplay = 0;
    this.secondsDisplay = 0;
  }

  private startTimer() {
    this.hoursDisplay = 0;
    this.minutesDisplay = 0;
    this.secondsDisplay = 0;
    let timer = Observable.timer(1, 1000);
    this.sub = timer.subscribe(
      t => {
        this.ticks = t;

        this.secondsDisplay = this.getSeconds(this.ticks);
        this.minutesDisplay = this.getMinutes(this.ticks);
        this.hoursDisplay = this.getHours(this.ticks);
        // console.log(this.secondsDisplay)
        if (this.secondsDisplay % 10 == 0 && this.secondsDisplay != 0) {
          this.getBreakdownIdle();
          this.getStatusPWR();
        }
        if (this.secondsDisplay % 15 == 0 && this.secondsDisplay != 0) {
          this.sendOF();
        }
      }
    );
  }

  //Method Mencari Idle
  getBreakdownIdle() {
    this.breakdownService.findBreakdownIdle(this.informasi.nik).subscribe(
      status => {
        this.StatusIdleBreakdown = status;
        console.log(this.StatusIdleBreakdown);
        // console.log(this.StatusIdleBreakdown[0].noLinePeseé);
        //console.log(this.StatusIdleBreakdown[0].status);
        if (this.StatusIdleBreakdown[0].status == "IDLE") {
          this.potService.NoLinePesee1 = this.StatusIdleBreakdown[0].noLinePeseé;
          this.potService.Equipment = this.informasi.WB;
          this.potService.LoginName = this.informasi.nik;
          this.potService.Shift = this.informasi.Shift;
          this.potService.Tanggal = new Date();
          this.potService.Start = new Date();
          if (this.potService.Status == 'Idle' && this.potService.NoLinePesee1 != this.potService.NoLinePesee2) {
            this.potService.NoLinePesee2 = this.potService.NoLinePesee1;
            this.openPOT();
            console.log("Dari POT Service : " + this.potService.NoLinePesee1);
            console.log("Dari POT Service : " + this.potService.Equipment);
            console.log("Dari POT Service : " + this.potService.LoginName);
            console.log("Dari POT Service : " + this.potService.Shift);
            console.log("Dari POT Service : " + this.potService.Tanggal);
            console.log("Dari POT Service : " + this.potService.Start);
          }
        }
      },
      err => {
        console.log(err);
      }
    );

  }

  //Method Mencari Idle
  getStatusPWR() {
    this.breakdownService.findStatusPWR().subscribe(
      status => {
        this.StatusPWR = status;
        console.log(this.StatusPWR)
        console.log(this.StatusPWR.rm)
        this.statusRM = this.StatusPWR.rm;
        this.statusWEIGH = this.StatusPWR.weigh / 1000;
        this.statusPACK = this.StatusPWR.pack;

        this.widthWeigh = (this.statusWEIGH / this.informasi.OrderQuantity) * 100;
        this.widthRM = (this.statusRM / this.informasi.RMQuantity) * 100
        console.log("STATUS WEIGH : " + this.widthWeigh);
        console.log("STATUS RM : " + this.widthRM);

        if (this.widthRM < 98) {
          this.btnEndOf = "NO";
        } else if (this.widthRM >= 98) {
          this.btnEndOf = "YES"
        }

        console.log("RM : " + this.statusRM + " WEIGH : " + this.statusWEIGH + " PACK : " + this.statusPACK)

      },
      err => {
        console.log(err);
      }
    );

  }

  sendOF() {
    let userOF: UserOF = new UserOF(
    this.informasi.nik,
    this.informasi.WB,
    this.informasi.OF,
    this.informasi.Formula,
    this.statusWEIGH,
    this.statusRM,
    this.statusPACK,
    null,
    this.informasi.Shift,
    null
    );
    this.breakdownService.updateOF(userOF).subscribe();
  }

  endOf() {
    let userOF: UserOF = new UserOF(
      this.informasi.nik,
      this.informasi.WB,
      this.informasi.OF,
      this.informasi.Formula,
      this.statusWEIGH,
      this.statusRM,
      this.statusPACK,
      null,
      this.informasi.Shift,
      new Date()
    );
    this.breakdownService.saveOF(userOF).subscribe();
  }

  //===============UNTUK TIMER=====================
  private getSeconds(ticks: number) {
    return this.pad(ticks % 60);
  }

  private getMinutes(ticks: number) {
    return this.pad((Math.floor(ticks / 60)) % 60);
  }

  private getHours(ticks: number) {
    return this.pad(Math.floor((ticks / 60) / 60));
  }

  private pad(digit: any) {
    return digit <= 9 ? '0' + digit : digit;
  }

}
