import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EfisiensiComponent } from './efisiensi.component';

describe('EfisiensiComponent', () => {
  let component: EfisiensiComponent;
  let fixture: ComponentFixture<EfisiensiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EfisiensiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EfisiensiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
