import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BreakdownService } from '../../service/breakdown.service';
import { PotService } from '../../service/pot.service';
import { Observable, Subscription } from 'rxjs/Rx';
import { InformasiService } from '../../service/informasi.service';
import { PieModel } from '../../model/PieModel';
declare var google: any;

@Component({
  selector: 'app-efisiensi',
  templateUrl: './efisiensi.component.html',
  styleUrls: ['./efisiensi.component.css']
})
export class EfisiensiComponent implements OnInit, OnDestroy {

    private pieData: PieModel = {description: null, total: null};
    private pieArray: {description: string, total: number}[]=[];
    tampung: string[] = [];
    tampTot: number[]=[];
    description: string="";
    total: number=null;

    // pieTampung: [string, number];
    pieTampung: any[]=[];

    test: any[] = [['Net WT', 4],
    ['Interval Between Packing', 2],
    ['Preparation', 1],
    ['Storing', 1],
    ['Waiting RM', 1],
    ['Breakdown', 1],
    ['Undefined', 1]]

    //Untuk Timer
    ticks = 0;
    minutesDisplay: number = 0;
    hoursDisplay: number = 0;
    secondsDisplay: number = 0;
    sub: Subscription;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private breakdownService: BreakdownService,
    private potService: PotService,
    private informasi: InformasiService) { }

  ngOnDestroy() {
    this.stopTimer();
    this.sub.unsubscribe();
  }

  ngOnInit() {
    // this.pieChart(this.test);
    this.startTimer();
    console.log("DATA YANG DIBUTUHKAN UNTUK EFISIENSI CHART : "+this.informasi.nik+"DAN SHIFT : "+this.informasi.Shift)
    this.getPieData();
  }

  pieChart(tampung){
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

      // Create the data table.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Topping');
      data.addColumn('number', 'Slices');
      data.addRows(tampung);

      // Set chart options
      var options = {'width':600,
                     'height':350,
                      colors: ['#26c14a', '#4198f4', '#e534ca', '#e53439', '#f6c7b6', '#f48f41','#1a093f']};

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.PieChart(document.getElementById('pieChart'));
      chart.draw(data, options);
    }

  }

  getPieData(){
    this.breakdownService.getPie(this.informasi.nik, this.informasi.Shift).subscribe(
      status => {
        this.pieArray = status;
        // console.log(this.dataGantt.description)
        console.log(this.pieArray)
        for(var i = 0;i<this.pieArray.length;i++) {
            this.tampung[i] = this.pieArray[i].description
        }
        
        console.log("TAMPUNG")
        console.log(this.tampung)
        console.log(this.tampTot)
        for(var s=0;s<this.pieArray.length;s++){
            this.description = this.pieArray[s].description;
            this.total = this.pieArray[s].total;
            // this.tampTot[s]=this.pieArray[s].total
            this.pieTampung[s]=[this.description, this.total]
            // console.log([this.pieArray[s].total])
        }
        console.log("TAMPILIN AJA")
        console.log(this.pieTampung)
        this.pieChart(this.pieTampung);

      },
      err => {
        console.log(err);
      }
    );
}

private stopTimer() {
  this.sub.unsubscribe();
  this.hoursDisplay = 0;
  this.minutesDisplay = 0;
  this.secondsDisplay = 0;
}

private startTimer() {
  this.hoursDisplay = 0;
  this.minutesDisplay = 0;
  this.secondsDisplay = 0;
  let timer = Observable.timer(1, 1000);
  this.sub = timer.subscribe(
    t => {
      this.ticks = t;

      this.secondsDisplay = this.getSeconds(this.ticks);
      this.minutesDisplay = this.getMinutes(this.ticks);
      this.hoursDisplay = this.getHours(this.ticks);
      // console.log(this.secondsDisplay)
      if (this.secondsDisplay % 30 == 0 && this.secondsDisplay != 0){
        this.getPieData();
      }
    }
  );
}



//===============UNTUK TIMER=====================
private getSeconds(ticks: number) {
  return this.pad(ticks % 60);
}

private getMinutes(ticks: number) {
  return this.pad((Math.floor(ticks / 60)) % 60);
}

private getHours(ticks: number) {
  return this.pad(Math.floor((ticks / 60) / 60));
}

private pad(digit: any) {
  return digit <= 9 ? '0' + digit : digit;
}

}
