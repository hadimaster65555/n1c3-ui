import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BreakdownService } from '../../service/breakdown.service';
import { PotService } from '../../service/pot.service';
import { InformasiService } from '../../service/informasi.service';
import { Gantt } from '../../model/Gantt';
import { Observable, Subscription } from 'rxjs/Rx';
declare var google: any;

@Component({
  selector: 'app-gantt',
  templateUrl: './gantt.component.html',
  styleUrls: ['./gantt.component.css']
})
export class GanttComponent implements OnInit, OnDestroy {
  
  private test: Date = new Date();
  private waktu: number = this.test.getTime()
  
  private ganttData: Gantt = { description: "Preparation", start: this.test, stop: null };
  // private ganttData: Gantt = { description: "preparation", start: new Date(1516160828117), stop: new Date(1516160863437) };

  private dataGanttArray: {description: string, start: Date, stop: Date}[]=[];
  private parsingGantt: {description: string, start: Date, stop: Date}[]=[];

  description: string="";
  start: Date=null;
  stop: Date=null;
  gantTampung: any[]=[];

  //Untuk Timer
  ticks = 0;
  minutesDisplay: number = 0;
  hoursDisplay: number = 0;
  secondsDisplay: number = 0;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private breakdownService: BreakdownService,
    private potService: PotService,
    private informasi: InformasiService) { }

  ngOnDestroy() {
    this.stopTimer();
    this.sub.unsubscribe();
    console.log("STOP TIMER GANTT")
  }

  ngOnInit() {
    this.startTimer();
    this.getGanttDataArray();
    console.log("Yang Dibutuhkan Untuk GANTT CHART " + this.informasi.nik)
    let values = [
      ['Net WT', new Date(0, 0, 0, 7, 15, 0), new Date(0, 0, 0, 7, 19, 0)],
      ['Net WT', new Date(0, 0, 0, 7, 21, 0), new Date(0, 0, 0, 7, 25, 0)],
      ['Net WT', new Date(0, 0, 0, 7, 32, 0), new Date(0, 0, 0, 7, 36, 0)],
      ['Net WT', new Date(0, 0, 0, 7, 46, 0), new Date(0, 0, 0, 7, 50, 0)],
      ['Net WT', new Date(0, 0, 0, 7, 58, 0), new Date(0, 0, 0, 8, 2, 0)],
      ['Interval Between packing', new Date(0, 0, 0, 7, 19, 0), new Date(0, 0, 0, 7, 21, 0)],
      ['Interval Between packing', new Date(0, 0, 0, 7, 25, 0), new Date(0, 0, 0, 7, 27, 0)],
      ['Interval Between packing', new Date(0, 0, 0, 7, 55, 0), new Date(0, 0, 0, 7, 58, 0)],
      ['Preparation', new Date(0, 0, 0, 7, 0, 0), new Date(0, 0, 0, 7, 15, 0)],
      ['Storing', new Date(0, 0, 0, 7, 36, 0), new Date(0, 0, 0, 7, 46, 0)],
      ['Waiting RM', new Date(0, 0, 0, 7, 50, 0), new Date(0, 0, 0, 7, 55, 0)],
      ['Breakdown', new Date(0, 0, 0, 8, 2, 0), new Date(0, 0, 0, 8, 7, 0)],
      ['Testing', new Date(0, 0, 0, 8, 2, 0), new Date(0, 0, 0, 8, 7, 0)]

    ]
    // this.gantt(values);
    // console.log(this.parsingGantt)
  }

  gantt(values) {
    google.charts.load("current", { packages: ["timeline"] });
    google.charts.setOnLoadCallback(drawChart);
    // Variable untuk parsing ke Gantt  

    function drawChart() {
      var container = document.getElementById('ganttChart');
      var chart = new google.visualization.Timeline(container);
      var dataTable = new google.visualization.DataTable();
      // dataTable.addColumn({ type: 'string', id: 'Room' });
      dataTable.addColumn({ type: 'string', id: 'Description' });
      dataTable.addColumn({ type: 'date', id: 'Start' });
      dataTable.addColumn({ type: 'date', id: 'End' });
      dataTable.addRows(values);

      var options = {
        timeline: { colorByRowLabel: true },
        width: 800,
        height: 300,
        colors: ['#26c14a', '#4198f4', '#e534ca', '#e53439', '#f6c7b6', '#f48f41','#1a093f'],
      };

      chart.draw(dataTable, options);
    }
  }

  getGanttDataArray(){
     this.breakdownService.getGanttArray(this.informasi.nik).subscribe(
      status => {
        this.dataGanttArray = status;
        console.log("DATA GANTT ARRAY : ")
        console.log(this.dataGanttArray);
        for(var i = 0;i<this.dataGanttArray.length;i++) {
          this.parsingGantt[i] = this.dataGanttArray[i]
          this.description = this.dataGanttArray[i].description
          this.start = this.dataGanttArray[i].start
          this.stop = this.dataGanttArray[i].stop
          this.gantTampung[i]=[this.description, new Date(this.start), new Date(this.stop)]
        }
        console.log("Hasil Parsing Data Array : ")
        console.log(this.gantTampung)
        this.gantt(this.gantTampung);

      },
      err => {
        console.log(err);
      }
    );
}

private stopTimer() {
  this.sub.unsubscribe();
  this.hoursDisplay = 0;
  this.minutesDisplay = 0;
  this.secondsDisplay = 0;
}

private startTimer() {
  this.hoursDisplay = 0;
  this.minutesDisplay = 0;
  this.secondsDisplay = 0;
  let timer = Observable.timer(1, 1000);
  this.sub = timer.subscribe(
    t => {
      this.ticks = t;

      this.secondsDisplay = this.getSeconds(this.ticks);
      this.minutesDisplay = this.getMinutes(this.ticks);
      this.hoursDisplay = this.getHours(this.ticks);
      // console.log(this.secondsDisplay)
      if (this.secondsDisplay % 10 == 0 && this.secondsDisplay != 0){
        this.getGanttDataArray();
      }
    }
  );
}

//===============UNTUK TIMER=====================
private getSeconds(ticks: number) {
  return this.pad(ticks % 60);
}

private getMinutes(ticks: number) {
  return this.pad((Math.floor(ticks / 60)) % 60);
}

private getHours(ticks: number) {
  return this.pad(Math.floor((ticks / 60) / 60));
}

private pad(digit: any) {
  return digit <= 9 ? '0' + digit : digit;
}

}
