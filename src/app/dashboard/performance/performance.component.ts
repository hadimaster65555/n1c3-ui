import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs/Rx';
import { BreakdownService } from '../../service/breakdown.service';
import { PotService } from '../../service/pot.service';
import { InformasiService } from '../../service/informasi.service';
import { Performance, Shift } from '../../model/Performance';
declare var google: any;

@Component({
  selector: 'app-performance',
  templateUrl: './performance.component.html',
  styleUrls: ['./performance.component.css']
})
export class PerformanceComponent implements OnInit, OnDestroy {

  // Untuk Performance
  private performanceData : Performance = new Performance(
    new Shift(null, null, null, null),
    new Shift(null, null, null, null),
    new Shift(null, null, null, null)
);

  //Untuk Timer
  ticks = 0;
  minutesDisplay: number = 0;
  hoursDisplay: number = 0;
  secondsDisplay: number = 0;
  sub: Subscription;

  //Untuk Chart
  var: number = 4;
  net1: Date = new Date(0, 0, 0, 13, 0, 0);

  // Inisialisasi Untuk PERFORMANCE RM, PACK dan OF
  OFs1: number = null;
  OFs2: number = null;
  OFs3: number = null;

  RMs1: number = null;
  RMs2: number = null;
  RMs3: number = null;

  PackS1: number = null;
  PackS2: number = null;
  PackS3: number = null;

  Efisiensi1: number = null;
  Efisiensi2: number = null;
  Efisiensi3: number = null;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private breakdownService: BreakdownService,
    private potService: PotService,
    private informasiService: InformasiService) { }

  ngOnDestroy() {
    this.stopTimer();
    this.sub.unsubscribe();
  }

  ngOnInit() {
    this.startTimer();
    this.getPerformance();
    this.RmChart();
    this.packChart();
    this.efisiensiChart();
    this.OfChart();
    // this.RmChart();
    // this.packChart();
    // this.OfChart();
    // this.efisiensiChart();
    // this.RmChartModal();
    // this.packChartModal();
    // this.OfChartModal();
    // this.efisiensiChartModal();
    // this.performanceChart();
  }

  performanceChart() {
    google.charts.load('current', { 'packages': ['bar'] });
    google.charts.setOnLoadCallback(drawChart);
    var OFshift_1 = this.OFs1;
    var OFshift_2 = this.OFs2;
    var OFshift_3 = this.OFs3;

    var RM1 = this.RMs1;
    var RM2 = this.RMs2;
    var RM3 = this.RMs3;

    var Pack1 = this.PackS1;
    var Pack2 = this.PackS2;
    var Pack3 = this.PackS3;

    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Shift', 'OF', 'RM', 'Pack', 'Efisiensi'],
        ['Shift 1', OFshift_1, RM1, Pack1, 10],
        ['Shift 2', OFshift_2, RM2, Pack2, 10],
        ['Shift 3', OFshift_3, RM3, Pack3, 10]
      ]);

      var options = {
        chart: {
          title: '',
          subtitle: ''
        },
        width: 300,
        height: 150,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: false
      };

      var chart = new google.charts.Bar(document.getElementById('performance'));

      chart.draw(data, google.charts.Bar.convertOptions(options));
    }
  }

  RmChart() {
    google.charts.load('current', { 'packages': ['bar'] });
    google.charts.setOnLoadCallback(drawChart);

    var RM1 = this.RMs1;
    var RM2 = this.RMs2;
    var RM3 = this.RMs3;

    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Shift', 'RM'],
        ['Shift 1', RM1],
        ['Shift 2', RM2],
        ['Shift 3', RM3]
      ]);

      var options = {
        chart: {
          title: 'RM',
          subtitle: ''
        },
        width: 300,
        height: 150,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: false,
        colors: ['#26c14a']
      };

      var chart = new google.charts.Bar(document.getElementById('rmChart'));

      chart.draw(data, google.charts.Bar.convertOptions(options));
    }
  }

  RmChartModal() {
    google.charts.load('current', { 'packages': ['bar'] });
    google.charts.setOnLoadCallback(drawChart);

    var RM1 = this.RMs1;
    var RM2 = this.RMs2;
    var RM3 = this.RMs3;

    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Shift', 'RM'],
        ['Shift 1', RM1],
        ['Shift 2', RM2],
        ['Shift 3', RM3]
      ]);

      var options = {
        chart: {
          title: '',
          subtitle: ''
        },
        width: 300,
        height: 150,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: false,
        colors: ['#26c14a']
      };

      var chart = new google.charts.Bar(document.getElementById('rmChartModal'));

      chart.draw(data, google.charts.Bar.convertOptions(options));
    }
  }

  OfChart() {
    google.charts.load('current', { 'packages': ['bar'] });
    google.charts.setOnLoadCallback(drawChart);
    var OFshift_1 = this.OFs1;
    var OFshift_2 = this.OFs2;
    var OFshift_3 = this.OFs3;
    // console.log("Dari OF Cahrt : "+OFshift_1);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Shift', 'OF'],
        ['Shift 1', OFshift_1],
        ['Shift 2', OFshift_2],
        ['Shift 3', OFshift_3]
      ]);

      var options = {
        chart: {
          title: 'OF',
          subtitle: ''
        },
        width: 300,
        height: 150,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: false,
        colors: ['#4198f4']
      };

      var chart = new google.charts.Bar(document.getElementById('ofChart'));

      chart.draw(data, google.charts.Bar.convertOptions(options));
    }
  }

  OfChartModal() {
    google.charts.load('current', { 'packages': ['bar'] });
    google.charts.setOnLoadCallback(drawChart);
    var OFshift_1 = this.OFs1;
    var OFshift_2 = this.OFs2;
    var OFshift_3 = this.OFs3;
    
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Shift', 'OF'],
        ['Shift 1', OFshift_1],
        ['Shift 2', OFshift_2],
        ['Shift 3', OFshift_3]
      ]);

      var options = {
        chart: {
          title: '',
          subtitle: ''
        },
        width: 300,
        height: 150,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: false,
        colors: ['#4198f4']
      };

      var chart = new google.charts.Bar(document.getElementById('ofChartModal'));

      chart.draw(data, google.charts.Bar.convertOptions(options));
    }
  }

  packChart() {
    google.charts.load('current', { 'packages': ['bar'] });
    google.charts.setOnLoadCallback(drawChart);
  
    var Pack1 = this.PackS1;
    var Pack2 = this.PackS2;
    var Pack3 = this.PackS3;
    console.log("Dari PACK CHART : "+Pack1)
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Shift', 'Pack'],
        ['Shift 1', Pack1],
        ['Shift 2', Pack2],
        ['Shift 3', Pack3]
      ]);

      var options = {
        chart: {
          title: 'PACK',
          subtitle: ''
        },
        width: 300,
        height: 150,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: false,
        colors: ['#e534ca']
      };

      var chart = new google.charts.Bar(document.getElementById('packChart'));

      chart.draw(data, google.charts.Bar.convertOptions(options));
    }
  }

  packChartModal() {
    google.charts.load('current', { 'packages': ['bar'] });
    google.charts.setOnLoadCallback(drawChart);
  
    var Pack1 = this.PackS1;
    var Pack2 = this.PackS2;
    var Pack3 = this.PackS3;
    
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Shift', 'Pack'],
        ['Shift 1', Pack1],
        ['Shift 2', Pack2],
        ['Shift 3', Pack3]
      ]);

      var options = {
        chart: {
          title: '',
          subtitle: ''
        },
        width: 300,
        height: 150,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: false,
        colors: ['#e534ca']
      };

      var chart = new google.charts.Bar(document.getElementById('packChartModal'));

      chart.draw(data, google.charts.Bar.convertOptions(options));
    }
  }  


  efisiensiChart() {
    google.charts.load('current', { 'packages': ['bar'] });
    google.charts.setOnLoadCallback(drawChart);

    var efisiensi1 = this.Efisiensi1;
    var efisiensi2 = this.Efisiensi2;
    var efisiensi3 = this.Efisiensi3;

    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Shift', 'Efisiensi'],
        ['Shift 1', efisiensi1],
        ['Shift 2', efisiensi2],
        ['Shift 3', efisiensi3]
      ]);

      var options = {
        chart: {
          title: 'EFISIENSI',
          subtitle: ''
        },
        width: 300,
        height: 150,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: false,
        colors: ['#e53439']
      };

      var chart = new google.charts.Bar(document.getElementById('efisiensiChart'));

      chart.draw(data, google.charts.Bar.convertOptions(options));
    }
  }

  efisiensiChartModal() {
    google.charts.load('current', { 'packages': ['bar'] });
    google.charts.setOnLoadCallback(drawChart);

    var efisiensi1 = this.Efisiensi1;
    var efisiensi2 = this.Efisiensi2;
    var efisiensi3 = this.Efisiensi3;

    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Shift', 'Efisiensi'],
        ['Shift 1', efisiensi1],
        ['Shift 2', efisiensi2],
        ['Shift 3', efisiensi3]
      ]);

      var options = {
        chart: {
          title: '',
          subtitle: ''
        },
        width: 300,
        height: 150,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: false,
        colors: ['#e53439']
      };

      var chart = new google.charts.Bar(document.getElementById('efisiensiChartModal'));

      chart.draw(data, google.charts.Bar.convertOptions(options));
    }
  }


  // GET PERFORMANCE
  getPerformance(){
    this.breakdownService.performance().subscribe(
      status => {
        this.performanceData = status;
        console.log(status);
        console.log("SHIF 1 AJA : ");
        console.log(this.performanceData.shift1.ofShift);

        this.OFs1 = this.performanceData.shift1.ofShift;
        this.OFs2 = this.performanceData.shift2.ofShift;
        this.OFs3 = this.performanceData.shift3.ofShift;

        this.RMs1 = this.performanceData.shift1.rmShift;
        this.RMs2 = this.performanceData.shift2.rmShift;
        this.RMs3 = this.performanceData.shift3.rmShift;

        this.PackS1 = this.performanceData.shift1.packShift;
        this.PackS2 = this.performanceData.shift2.packShift;
        this.PackS3 = this.performanceData.shift3.packShift;

        this.Efisiensi1 = this.performanceData.shift1.efisiensi;
        this.Efisiensi2 = this.performanceData.shift2.efisiensi;
        this.Efisiensi3 = this.performanceData.shift3.efisiensi;

        console.log ("Pack Shift 1 = " + this.PackS1)
        
        this.performanceChart();
        this.RmChart();
        this.packChart();
        this.OfChart();
        this.efisiensiChart();
        this.RmChartModal();
        this.packChartModal();
        this.OfChartModal();
        this.efisiensiChartModal();
        // console.log("RM : "+this.statusRM+" WEIGH : "+this.statusWEIGH+" PACK : "+this.statusPACK)

      },
      err => {
        console.log(err);
      }
    );
  }

  rmChartTrig(){
    this.RmChartModal();
  }


  private stopTimer() {
    this.sub.unsubscribe();
    this.hoursDisplay = 0;
    this.minutesDisplay = 0;
    this.secondsDisplay = 0;
  }

  private startTimer() {
    this.hoursDisplay = 0;
    this.minutesDisplay = 0;
    this.secondsDisplay = 0;
    let timer = Observable.timer(1, 1000);
    this.sub = timer.subscribe(
      t => {
        this.ticks = t;

        this.secondsDisplay = this.getSeconds(this.ticks);
        this.minutesDisplay = this.getMinutes(this.ticks);
        this.hoursDisplay = this.getHours(this.ticks);
        // console.log(this.secondsDisplay)
        if (this.secondsDisplay % 30 == 0 && this.secondsDisplay != 0){
          this.getPerformance();
        }
      }
    );
  }



  //===============UNTUK TIMER=====================
  private getSeconds(ticks: number) {
    return this.pad(ticks % 60);
  }

  private getMinutes(ticks: number) {
    return this.pad((Math.floor(ticks / 60)) % 60);
  }

  private getHours(ticks: number) {
    return this.pad(Math.floor((ticks / 60) / 60));
  }

  private pad(digit: any) {
    return digit <= 9 ? '0' + digit : digit;
  }

}
