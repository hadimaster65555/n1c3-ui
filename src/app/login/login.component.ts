import { Component, OnInit } from '@angular/core';
import { InformasiService } from '../service/informasi.service';
import { Router } from '@angular/router';
import { Login } from './Login';
import { BreakdownService } from '../service/breakdown.service';
import { RestsourceService } from '../service/restsource.service';
import { AuthenticationService } from '../service/authentication.service';
import { LoginModel } from '../model/loginmodel';
import { Subscription } from 'rxjs/Subscription';
import { TokenstorageService } from '../service/tokenstorage.service';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-login',
  providers: [AuthenticationService, TokenstorageService],
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  // username: String;
  // password: String;
  // public info: any = {};
  nama: String;
  statusLogin: Login[] = [];
  loginData: LoginModel;
  idusername: string;
  idpassword: string;
  public errorMsg = '';

  sub: Subscription = null;

  constructor(
    private informasi: InformasiService,
    private router: Router,
    // private breakdownService: BreakdownService,
    private restSourceService: RestsourceService,
    private authenticationService: AuthenticationService) {
  }


  ngOnInit() {
  }

  login() {
    console.log('calling submit...');
    const headers = new HttpHeaders()
      .set('Authorization', 'Basic ' + btoa(this.idusername + ':' + this.idpassword));
    console.log('attempAuth ::');
    console.log(headers);
    this.restSourceService.getAvailable(this.idusername, this.idpassword)
      .subscribe((data) => {
        console.log(data);
        if (data === null) {
          this.errorMsg = 'NIK dan/atau Password yang Anda masukkan salah, silahkan cek kembali.';
          console.log('stop');
        } else {
          this.errorMsg = '';
          this.restSourceService.getUser(this.idusername).subscribe((userdata) => {
            this.loginData = userdata;
            console.log('namanya si ' + this.loginData.nama);
            const info = { nik: btoa(this.idusername), nama: btoa(this.loginData.nama), login: Date.now() };
            localStorage.setItem('$2a$10$eKXxFtLgnQAoQYhtccIj5egtPHD7yFslrXeIDtMnyNxeMV.NzC51C', JSON.stringify(info));
            console.log('nik nya ' + this.idusername);
            localStorage.setItem('$2a$10$eKXxFtLgnQAoQYhtccIj5egtPHD7yFslrXeIDtMnyNxeMV',
              JSON.stringify({ user: data, authenticated: Boolean(data) }));
            this.router.navigate(['spi-first']);
          });
        }
      });
  }
  //     .subscribe((data) => {
  // this.informasi.nik = this.idusername;
  // this.restSourceService.getUser(this.idusername, this.pass).subscribe((userdata) => {
  // this.loginData = userdata;
  // this.informasi.nama = this.loginData.nama;
  // console.log('namanya si ' + this.informasi.nama);
  // const info = { nik: btoa(this.informasi.nik), nama: btoa(this.informasi.nama), login: Date.now()};
  // localStorage.setItem('$2a$10$eKXxFtLgnQAoQYhtccIj5egtPHD7yFslrXeIDtMnyNxeMV.NzC51C', JSON.stringify(info));
  // console.log('nik nya ' + this.informasi.nik);
  // this.informasi.nama = this.loginData.nama;
  // console.log('nama nya ' + this.informasi.nama);
  // this.router.navigate(['spi-first']);
  // });
  //     },
  //     (err) => {
  //       this.errorMsg = 'NIK dan/atau Password yang Anda masukkan salah, silahkan cek kembali.';
  //       return;
  //     });
  //   }
  // }
  // 
  // login() {
  //   if (!this.authenticationService.login(this.idusername, this.idpassword)) {
  //     this.errorMsg = 'Tidak bisa masuk, periksa kembali nik dan password anda.';
  //   } else {
  //     this.errorMsg = 'username dan password benar, silahkan masuk.';
  //   }
  // console.log("Username Login " + this.info.username);
  // this.informasi.nik = this.info.username;
  // this.informasi.password = this.info.password;
  // this.getLogin();
  // this.router.navigate(['/spi-first']);
  // }

  // Method Login
  // getLogin() {
  //   // this.informasi.nik = this.info.username;
  //   this.informasi.password = this.info.password;
  //   console.log('Username ' + this.informasi.nik);
  //   console.log('Password ' + this.informasi.password);
  //   console.log('Equipment ' + this.informasi.WB);
  //   this.breakdownService.login().subscribe(
  //     login => {
  //       this.statusLogin = login;
  //       console.log(this.statusLogin);
  //       // if (this.StatusIdleBreakdown[0].status == "IDLE"){
  //       //   this.potService.NoLinePesee1 = this.StatusIdleBreakdown[0].noLinePeseé;
  //       //   this.potService.Equipment = this.informasiService.WB;
  //       //   this.potService.LoginName = this.informasiService.nik;
  //       //   this.potService.Shift = this.informasiService.Shift;
  //       //   this.potService.Tanggal = new Date();
  //       //   this.potService.Start = new Date();
  //       //   if (this.potService.Status == 'Idle' && this.potService.NoLinePesee1 != this.potService.NoLinePesee2){
  //       //       this.potService.NoLinePesee2 = this.potService.NoLinePesee1;
  //       //       this.openPOT();
  //       //       console.log("Dari POT Service : "+this.potService.NoLinePesee1);
  //       //       console.log("Dari POT Service : "+this.potService.Equipment);
  //       //       console.log("Dari POT Service : "+this.potService.LoginName);
  //       //       console.log("Dari POT Service : "+this.potService.Shift);
  //       //       console.log("Dari POT Service : "+this.potService.Tanggal);
  //       //       console.log("Dari POT Service : "+this.potService.Start);
  //       //   }
  //       // }
  //     },
  //     err => {
  //       console.log(err);
  //     }
  //   );

  // }


  // }
}


