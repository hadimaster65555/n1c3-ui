export class ActionType {
    
    constructor(
      public id: Number,
      public parentID: Number,
      public description: String) {
      }
      
}