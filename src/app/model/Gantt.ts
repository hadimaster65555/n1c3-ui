export class Gantt {
    
    constructor(
        public description: string,
        public start: Date,
        public stop: Date) {
      }
      
}