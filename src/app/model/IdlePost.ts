export class IdlePost {
    
  constructor(
    public NoLinePesee: Number,
    public Equipment: String,
    public Shift: Number,
    public LoginName: String,
    public Tanggal: Date,
    public Type: Number,
    public Start: Date,
    public Stop: Date,
    public Reason: String,
    public Status: String) {
    }
    
}
    