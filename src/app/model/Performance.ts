export class Performance {
    
    constructor(
        public shift1: Shift,
        public shift2: Shift,
        public shift3: Shift) {
        }
    }

export class Shift{
    constructor(public ofShift: number, public rmShift: number, public packShift: number, public efisiensi: number){

    }
}

// public shift1: {ofShift: number, rmShift: number, packShift: number},
// public shift2: {ofShift: number, rmShift: number, packShift: number},
// public shift3: {ofShift: number, rmShift: number, packShift: number}
