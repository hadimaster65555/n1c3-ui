export class UserOF {
    
  constructor(
    public NIK: String,
    public WeighingBox: String,
    public OFNo: Number,
    public Formula: String,
    public OF_Quantity: Number,
    public OF_RM: Number,
    public OF_Pack: Number,
    public OF_Achievement: Number,
    public Shift: Number,
    public EndOFTime: Date) {
    }
    
}

//     public String NIK;
//     public Timestamp Tanggal;
//     public String WeighingBox;
//     public Double OFNo;
//     public String Formula;
//     public Double Shift;
//     public Double OrderQuantity;
//     public Double RMQuantity;
//     public Double OF_Quantity;
//     public Double OF_RM;
//     public Double OF_Pack;
//     public Double OF_Achievement;
//     public Timestamp EndOFTime;