export class LorGetOfweighing {
    wipOrderNo: Number;
    productNo: String;
    medium: String;
    department: String;
    createdOn: Date;
    group: String;
    groupDesc: String;
    orderQuantity: Number;
    nombreLigneId: Date;
    nombreLignePP: Date;
    nombreLignePesee: Number;
    progressStatus: Number;
    statusOF: String;
    findePesee: Date;
}

