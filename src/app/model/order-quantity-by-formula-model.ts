export class OrderQuantityByFormulaModel {
    constructor(
        public productNo: String,
        public orderQuantity: number
    ) {}
}
