export class RmQuantityByFormula {
    constructor(
        public formula: String,
        public rmQuantity: number
    ) {}
}
