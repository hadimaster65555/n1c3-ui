export class Userofmodel {
    constructor(
        public nik: String,
        public tanggal: Number,
        public weighingBox: String,
        public ofNo: Number,
        public formula: String,
        public shift: Number,
        public orderQuantity: Number,
        public rmQuantity: Number
    ) {}
}
