import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchformulaPipe } from './searchformula.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SearchformulaPipe],
  exports: [SearchformulaPipe]
})
export class SearchpipemoduleModule { }
