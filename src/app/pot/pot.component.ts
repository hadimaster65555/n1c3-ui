import {Component, OnInit} from '@angular/core';
import {PotService} from '../service/pot.service';
import {InformasiService} from '../service/informasi.service';
import { BreakdownService } from '../service/breakdown.service';

@Component({
  selector: 'app-pot',
  templateUrl: './pot.component.html',
  styleUrls: ['./pot.component.css']
})
export class PotComponent implements OnInit {

  type: String = '';
  lvlType: number;
  reason: String = '';


  //Variabel Untuk RM, PACK dan WEIGH
  statusRM: number;
  statusPACK: number;
  statusWEIGH: number;
  jumlahRM: number;
  totalWeigh: number;

  btnEndOf: String;
  widthRM: number;
  widthWeigh: number;

  // LEVEL
  public At: number;
  private AksiTipe: {id: Number, parentID: Number, description: String}[] = []
  public Level1: {id: Number, parentID: Number, description: String}[] = []
  private Level2: {id: Number, parentID: Number, description: String}[] = []
  private LevelDua: {id: Number, parentID: Number, description: String}[] = []

  

  statusKlik: String;

  // Informasi Login
  nik: String;
  namaOperator: String;
  Shift: number;
  Formula: String;
  Of: Number;

  pilihanSatu: string[] = [
    'Preparation', 'Storing', 'Waiting', 'Breakdown'
  ]
  pilihanPreparation: string[] = ['Administrasi', 'Persiapan Awal Shift', 'Clearing Akhir Shift', 'Persiapan Palet','Persiapan Packing','Change Over']
  pilihanStoring: string[] = ['Storing Weigh RM']
  pilihanWaiting: string[] = ['Waiting RM','Waiting Weighing Box']
  pilihanBreakdown: string[] = ['BD Timbangan','BD Booth','BD EHP/Manutan','BD Flashnet','Istirahat','Training']

  pilihanPertama: String ='';
  pilihanDua: String = '';

  constructor(private potService: PotService,
              private informasiService: InformasiService,
              private breakdownService: BreakdownService) {
  }

  ngOnInit() {
    this.statusPACK = this.potService.statusPACK;
    this.statusRM = this.potService.statusRM;
    this.statusWEIGH = this.potService.statusWEIGH;
    this.jumlahRM = this.potService.jumlahRM;
    this.totalWeigh = this.potService.totalWeigh;
    this.widthRM = this.potService.widthRM;
    this.widthWeigh = this.potService.widthWeigh;

    this.informasiLoad();
    this.statusKlik = this.potService.Status;
    this.getLevel();
    
    console.log(this.potService.Equipment);
    console.log(this.potService.LoginName);
    console.log(this.potService.Shift);
    console.log(this.potService.Tanggal);
    console.log(this.potService.Start);
    console.log(this.potService.Status);
  }

  submit(){
    this.potService.Type = (this.pilihanPertama+' / '+this.pilihanDua);
    this.potService.Reason = this.reason;
  }

  informasiLoad() {
    if (this.informasiService.nama == null) {
      this.namaOperator = 'Mattasir';
    } else {
      this.namaOperator = this.informasiService.nama;
    }
    if (this.informasiService.nik == null) {
      this.nik = 'J69511';
    } else {
      this.nik = this.informasiService.nik;
    }
    if (this.informasiService.Formula == null) {
      this.Formula = 'ZZ92502583';
    } else {
      this.Formula = this.informasiService.Formula;
    }
    if (this.informasiService.OF == null) {
      this.Of = 400000076099;
    } else {
      this.Of = this.informasiService.OF;
    }
    if (this.informasiService.Shift == null) {
      this.Shift = 1;
    } else {
      this.Shift = this.informasiService.Shift;
    }
  }

  btnPreparation(){
    this.pilihanPertama = 'Preparation'
  }
  btnWaiting(){
    this.pilihanPertama = 'Waiting'
  }
  btnStoring(){
    this.pilihanPertama = 'Storing'
  }
  btnBreakdown(){
    this.pilihanPertama = 'Breakdown'
  }

  levelType(lvl: number, lvlDes: string): void{
    //this.pilihanDua = prepare;
    this.lvlType = lvl;
    this.potService.ActionType = lvl
    this.pilihanDua = lvlDes
    console.log(lvl , lvlDes)
  }

  doSomething(prepare: number, prepareDes: string): void{
    //this.pilihanDua = prepare;
    this.At = prepare
    this.pilihanPertama = prepareDes
    this.potService.setParent(prepare)
    this.openLevel2();
  }

  openLevel2(){
    // for(var i = 0;i<this.AksiTipe.length;i++) {
    //   if (this.AksiTipe[i].parentID == level2){
    //     // console.log(this.AksiTipe[i])
    //     this.Level2[i] = this.AksiTipe[i]
    //   }
    // }
    // console.log("LEVEL 2")
    // console.log(this.Level2)

    console.log("Dari Service Parent "+this.potService.getParent())

    this.breakdownService.levelDua(this.At).subscribe(
      status => {
        this.Level2 = status;
        console.log(this.Level2);
        this.LevelDua = []
        for(var i = 0;i<this.Level2.length;i++) {
            console.log(this.Level2[i])
            this.LevelDua[i] = this.Level2[i]
        }
        console.log("Test")
        console.log(this.LevelDua)
      },
      err => {
        console.log(err);
      }
    );
  }

  getLevel() {
    this.breakdownService.type().subscribe(
      status => {
        this.AksiTipe = status;
        console.log(this.AksiTipe);
        // console.log(this.ActionType[0].id);
        for(var i = 0;i<this.AksiTipe.length;i++) {
          if (this.AksiTipe[i].parentID == 0){
            console.log(this.AksiTipe[i])
            this.Level1[i] = this.AksiTipe[i]
          }
        }
        console.log("Test")
        console.log(this.Level1)
      },
      err => {
        console.log(err);
      }
    );

  }
}
