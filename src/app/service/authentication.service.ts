import { Injectable, Inject } from '@angular/core';
import { Router, ActivatedRoute, UrlSegment } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { User } from '../model/user-model';
import { TokenstorageService } from './tokenstorage.service';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Credentials } from '../model/credentials';
import { Http } from '@angular/http';
import { RestsourceService } from './restsource.service';
import { LoginModel } from '../model/loginmodel';

interface State {
  user: User;
  authenticated: boolean;
}

const defaultState: State = {
  user: null,
  authenticated: false
};

@Injectable()
export class AuthenticationService {

  constructor(
    private http: HttpClient,
    private _http: Http,
    private jwt: TokenstorageService,
    private router: Router,
    private restSourceService: RestsourceService) {
  }

  loginData: LoginModel;

  attempAuth(credentials: Credentials) {
    
  }

  signout() {
    // reset the initial values
    localStorage.removeItem('$2a$10$eKXxFtLgnQAoQYhtccIj5egtPHD7yFslrXeIDtMnyNxeMV');
    localStorage.removeItem('$2a$10$eKXxFtLgnQAoQYhtccIj5egtPHD7yFslrXeIDtMnyNxeMV.NzC51C');
    this.router.navigate(['/login']);
  }

  checkCredentials() {

    if (localStorage.getItem('$2a$10$eKXxFtLgnQAoQYhtccIj5egtPHD7yFslrXeIDtMnyNxeMV') === null) {
      this.router.navigate(['login']);
    }
  }
}

// import { Injectable } from '@angular/core';
// import { Router } from '@angular/router';
// import { RestsourceService } from '../service/restsource.service';

// Model
// import { LoginModel } from '../model/loginmodel';
// import { InformasiService } from './informasi.service';

// @Injectable()
// export class AuthenticationService {

//   constructor(private router: Router,
//     private restSourceService: RestsourceService,
//     private informasi: InformasiService) { }

//   loginData: LoginModel;

//   logout() {
//     localStorage.removeItem('user');
//     this.router.navigate(['/login']);
//   }

//   login(username: string, password: string) {
    // this.informasi.nik = username;
    // this.restSourceService.getUser(username).subscribe((userdata) => {
    //   this.loginData = userdata;
    //   this.informasi.nama = this.loginData.nama;
    //   console.log('namanya si ' + this.informasi.nama);
    //   if (this.loginData.password === password) {
    //     console.log('password sama, lanjutkan');
    //     const data = { nik: this.informasi.nik, nama: this.informasi.nama, login: Date.now()};
    //     // const body = `username=${encodeURIComponent(this.loginData.username)}&nama=${encodeURIComponent(this.loginData.nama)}`;
    //     // const headers = new Headers();
    //     // headers.append('Content-Type', 'application/x-www-form-urlencoded');
    //     // headers.append('Authorization', 'Basic ' + btoa(username + ':' + password));
    //     localStorage.setItem('user', JSON.stringify(data));
    //     console.log('nik nya ' + this.informasi.nik);
    //     this.informasi.nama = this.loginData.nama;
    //     console.log('nama nya ' + this.informasi.nama);
//         this.router.navigate(['/spi-first']);
//         return true;
//       }
//       console.log('password tidak sama, jangan lanjutkan');
//       return false;
//     });
//   }


//   checkCredentials() {
//     if (localStorage.getItem('user') === null) {
//       this.router.navigate(['login']);
//     }
//   }
// }
