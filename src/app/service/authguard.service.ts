import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthenticationService) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    console.log('route::' + route.url);
    console.log('state::' + state.url);

  //   this.authService.checkCredentials().subscribe(auth => {
  //     if (!auth) {
  //       this.router.navigate(['login']);
  //       return false;
  //     }
  //   });
    return true;
  }
}
