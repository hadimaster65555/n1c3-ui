import { Injectable } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http/src/interceptor';
import { Router } from '@angular/router/src/router';
import { HttpRequest } from '@angular/common/http/src/request';
import { HttpHandler } from '@angular/common/http/src/backend';
import { Observable } from 'rxjs/Observable';
import { HttpProgressEvent,
         HttpResponse,
         HttpUserEvent,
         HttpEvent,
         HttpErrorResponse,
         HttpSentEvent,
         HttpHeaderResponse } from '@angular/common/http';
import { TokenstorageService } from './tokenstorage.service';

const TOKEN_HEADER_KEY = 'X-AUTH-TOKEN';

@Injectable()
export class AuthinterceptorService implements HttpInterceptor {

  constructor(private token: TokenstorageService, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent
    | HttpResponse<any> | HttpUserEvent<any>> {
    if (this.token.get()) {
      console.log('set token in header ::' + this.token.get());
    }

    return next.handle(req).do(
      (event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          const token = event.headers.get(TOKEN_HEADER_KEY);
          if (token) {
            console.log('saving token :: ' + token);
            this.token.save(token);
          }
        }
      }, (err: any) => {
        if (err instanceof HttpErrorResponse) {
          console.log(err);
          console.log('req url :: ' + req.url);
          if (!req.url.endsWith('/rest/**') && err.status === 401) {
            this.router.navigate(['/login']);
          }
        }
      }
    );
  }
}
