import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {InformasiService} from './informasi.service';
import {Login} from '../login/Login';
import { ActionType } from '../model/ActionType';
import { PotService } from './pot.service';
import { PotComponent } from '../pot/pot.component';
import { IdlePost } from '../model/IdlePost';
import { Idle } from '../model/Idle';
import { StatusPWR } from '../model/StatusPWR';
import { UserOF } from '../model/UserOF';
import { OFAchievement } from '../model/OFAchievement';
import { Gantt } from '../model/Gantt';
import { PieModel } from '../model/PieModel';

@Injectable()
export class BreakdownService {

  private apiUrl = 'rest/';

  constructor(private http: Http,
              private informasi: InformasiService,
              private potServeice: PotService) {

  }

  saveIdle(idlePost: IdlePost): Observable<IdlePost> {
    return this.http.post(this.apiUrl + 'push', idlePost)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  findBreakdownIdle(nik: String): Observable<Idle[]> {
    return this.http.get(this.apiUrl + 'idle/' + nik)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  login(): Observable<Login[]> {
    return this.http.get(this.apiUrl + 'login/' + this.informasi.nik)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  type(): Observable<ActionType[]>{
    return this.http.get(this.apiUrl + 'type')
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }
  
  levelDua(parent: number): Observable<ActionType[]>{
    console.log(this.potServeice.getParent())
    return this.http.get(this.apiUrl + 'levelDua/' + parent)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  findStatusPWR(): Observable<StatusPWR> {
    return this.http.get(this.apiUrl + 'status/' + this.informasi.OF)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  saveOF(userOF: UserOF): Observable<UserOF> {
    return this.http.post(this.apiUrl + 'stop/EndOF', userOF)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  updateOF(userOF: UserOF): Observable<UserOF> {
    return this.http.post(this.apiUrl + 'update/EndOF', userOF)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  achievementOF(): Observable<OFAchievement> {
    return this.http.get(this.apiUrl + 'achievement/' + this.informasi.nik)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  performance(){
    return this.http.get(this.apiUrl + 'performances')
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  getGanttArray(nik: string): Observable<Gantt[]>{
    return this.http.get(this.apiUrl + 'gantt/' + nik)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

  getPie(nik: string, shift: number): Observable<PieModel[]>{
    return this.http.get(this.apiUrl + 'pie/' + nik + "/"+ shift)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server Error'));
  }

}
