import { TestBed, inject } from '@angular/core/testing';

import { InformasiService } from './informasi.service';

describe('InformasiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InformasiService]
    });
  });

  it('should be created', inject([InformasiService], (service: InformasiService) => {
    expect(service).toBeTruthy();
  }));
});
