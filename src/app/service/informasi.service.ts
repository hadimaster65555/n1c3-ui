import { Injectable } from '@angular/core';

@Injectable()
export class InformasiService {

  constructor() { }
  nik: string;
  nama: string;
  Formula: String;
  OF: number;
  Shift: number;
  WB: string;
  password: String;
  OrderQuantity: number;
  RMQuantity: number;
}
