import { Injectable } from '@angular/core';

@Injectable()
export class PotService {

  NoLinePesee1: Number;
  NoLinePesee2: Number;
  Equipment: String;
  Shift: Number;
  LoginName: String;
  Tanggal: Date;
  Type: String;
  ActionType: Number;
  Start: Date;
  Stop: Date;
  Reason: String;
  Status: String;

  //Variabel Untuk RM, PACK dan WEIGH
  statusRM: number;
  statusPACK: number;
  statusWEIGH: number;
  jumlahRM: number;
  totalWeigh: number;

  widthRM: number;
  widthWeigh: number;

  private Parent: Number;


  constructor() { 
    this.Status='Idle';
  }

  setParent(p: Number){
    this.Parent = p;
    console.log("P "+this.Parent)
  }

  getParent(){
    console.log("GET PARENT POT SERVICE" + this.Parent)
    return this.Parent
  }

}
