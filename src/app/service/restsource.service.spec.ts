import { TestBed, inject } from '@angular/core/testing';

import { RestsourceService } from './restsource.service';

describe('RestsourceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RestsourceService]
    });
  });

  it('should be created', inject([RestsourceService], (service: RestsourceService) => {
    expect(service).toBeTruthy();
  }));
});
