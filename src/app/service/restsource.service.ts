import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http } from '@angular/http';

// Model
import { Userofmodel } from '../model/userofmodel';

// rxjs
import 'rxjs/add/operator/map';
import { Userlogin } from '../model/userlogin';
import { Observable } from 'rxjs/Observable';
import { Weighingboxmodel } from '../model/weighingboxmodel';
import { FormulaModel } from '../model/formula-model';
import { Response } from '@angular/http/src/static_response';
import { LoginModel } from '../model/loginmodel';
import { OrderQuantityByFormulaModel } from '../model/order-quantity-by-formula-model';
import { RmQuantityByFormula } from '../model/rm-quantity-by-formula';

@Injectable()
export class RestsourceService {
  public userOf: Userofmodel;

  constructor(private http: HttpClient,
    private _http: Http) { }
  //  Get WeighingBox Data from API
  getWeighingBoxData(): Observable<Weighingboxmodel> {
    return this.http.get<Weighingboxmodel>('/rest/weighingbox/list', { responseType: 'json' });
  }

  // Get Formula and OF Data from API
  getFormulaAndOfData(): Observable<FormulaModel> {
    return this.http.get<FormulaModel>('/rest/formulaAndOF/list', { responseType: 'json' });
  }

  // Get User Data from API
  getUser(nik: String): Observable<LoginModel> {
    return this.http.get<LoginModel>('/rest/processor/' + nik, { responseType: 'json' });
  }

  // Get User Available fro API
  getAvailable(nik: String, password: String): Observable<number> {
    return this.http.get<number>('/rest/processor/' + nik + '/' + password, { responseType: 'json' });
  }

  // Post User OF Data to API
  postUserOf(userOf: Userofmodel) {
    return this._http.post('/rest/userOf/add', userOf);
  }

  // Get Data OrderQuantity by Formula
  getOrderQuantityByFormula(productNo: String): Observable<OrderQuantityByFormulaModel> {
    return this.http.get<OrderQuantityByFormulaModel>('/rest/orderquantitybyformula/' + productNo, { responseType: 'json' });
  }

  // Get Data RMQuantity by Formula
  getRmQuantityByFormula(formula: String): Observable<RmQuantityByFormula> {
    return this.http.get<RmQuantityByFormula>('/rest/formula/' + formula, { responseType: 'json' });
  }

  // Get Formula By OF Number
  getFormulaByOfNumber(wipOrderNumber: Number): Observable<FormulaModel> {
    return this.http.get<FormulaModel>('/rest/formulaAndOf/' + wipOrderNumber, { responseType: 'json' });
  }

  // Data Setter for userOfModel
  dataSetterUserOf(userOf) {
    this.userOf = userOf;
  }

  // Data Getter for userOfModel
  dataGetterUserOf() {
    return this.userOf;
  }

  // Data Login Logout Poster
  postUserLogin(userLogin: Userlogin) {
    return this._http.post('/rest/userlogin/add', userLogin);
  }
}
