import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpiFirstComponent } from './spi-first.component';

describe('SpiFirstComponent', () => {
  let component: SpiFirstComponent;
  let fixture: ComponentFixture<SpiFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpiFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpiFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
