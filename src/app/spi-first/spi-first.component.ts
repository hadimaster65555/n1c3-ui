import { Component, OnInit, NgModule } from '@angular/core';
import { InformasiService } from '../service/informasi.service';
import { Router } from '@angular/router';
import { BreakdownService } from '../service/breakdown.service';
import { FormulaModel } from '../model/formula-model';
import { RestsourceService } from '../service/restsource.service';
import { Weighingboxmodel } from '../model/weighingboxmodel';
import { LorGetOfweighing } from '../model/lor-get-ofweighing';
import { Usermodel } from '../model/usermodel';
import { OrderQuantityByFormulaModel } from '../model/order-quantity-by-formula-model';
import { RmQuantityByFormula } from '../model/rm-quantity-by-formula';
import { Userofmodel } from '../model/userofmodel';
import { AuthenticationService } from '../service/authentication.service';
import { Userlogin } from '../model/userlogin';
import { TokenstorageService } from '../service/tokenstorage.service';

@Component({
  selector: 'app-spi-first',
  providers: [RestsourceService,
    AuthenticationService,
    InformasiService, TokenstorageService],
  templateUrl: './spi-first.component.html',
  styleUrls: ['./spi-first.component.css']
})

export class SpiFirstComponent implements OnInit {

  nik: String;
  nama: String;
  Formula1: String;
  Of: number;
  weighingBoxData: Weighingboxmodel;
  weighingBoxData2: Weighingboxmodel;
  formulaAndOfData: FormulaModel;
  informasiInput: any = {};
  informasiInputOf: number;
  infoFormula = '';
  data: any = [];
  selectedFormula: FormulaModel;
  userData: Usermodel;
  tanggalSekarang: Number;
  productNo: String;
  orderQuantityData: OrderQuantityByFormulaModel = new OrderQuantityByFormulaModel(null, null);
  rmQuantityData: RmQuantityByFormula = new RmQuantityByFormula(null, null);
  searchformula: any;
  userOf: Userofmodel = new Userofmodel(null, null, null, null, null, null, null, null);
  userLogin: Userlogin = new Userlogin(null, null, null, null);
  public errorMsg = '';

    constructor(private informasi: InformasiService,
      private breakdownService: BreakdownService,
      private restSourceService: RestsourceService,
      private router: Router,
      private authenticationService: AuthenticationService) { }

ngOnInit() {
  // Check Credentials (To Check Who The Hell Are You)
  // this.authenticationService.isAuthenticated();
  // Are you bring something I need?
  this.authenticationService.checkCredentials();

  // Fetch Data From Local Storage
  this.data = JSON.parse(localStorage.getItem('$2a$10$eKXxFtLgnQAoQYhtccIj5egtPHD7yFslrXeIDtMnyNxeMV.NzC51C'));
  this.informasi.nik = atob(this.data['nik']);
  this.informasi.nama = atob(this.data['nama']);
  this.nama = atob(this.data['nama']);
  this.nik = atob(this.data['nik']);

  // Weighing Box Data Getter
  this.restSourceService.getWeighingBoxData().subscribe(data => {
    this.weighingBoxData = data;
    console.log(this.weighingBoxData);
  });

  // Formula and OF Data Getter
  // this.restSourceService.getFormulaAndOfData().subscribe(resultArray =>
  //   this.formulaAndOfData = resultArray);
}

Submit() {
  this.informasi.Shift = this.informasiInput.Shift;
  this.informasi.WB = this.informasiInput.weightingBox;
  this.informasi.OF = this.informasiInputOf;
  this.informasi.Formula = this.selectedFormula.productNo;
  this.informasi.OrderQuantity = this.orderQuantityData.orderQuantity;
  this.informasi.RMQuantity = this.rmQuantityData.rmQuantity;
  this.productNo = this.informasi.Formula;
  // UserOf Data Poster Service
  this.userOf.nik = this.informasi.nik;
  this.userOf.tanggal = Date.now();
  this.userOf.weighingBox = this.informasi.WB;
  this.userOf.ofNo = this.informasiInputOf;
  this.userOf.formula = this.informasi.Formula;
  this.userOf.shift = this.informasi.Shift;
  localStorage.setItem('user-shift', JSON.stringify({
    shift: this.informasi.Shift,
    ofNo: this.informasiInputOf,
    formula: this.informasi.Formula,
    rm: this.informasi.RMQuantity,
    oq: this.informasi.OrderQuantity,
    wb: this.informasi.WB
  }));
  this.userOf.orderQuantity = this.orderQuantityData.orderQuantity;
  this.userOf.rmQuantity = this.rmQuantityData.rmQuantity;
  this.restSourceService.dataSetterUserOf(this.userOf);
  this.restSourceService.postUserOf(this.userOf).subscribe((userOf) => {
    console.log(this.userOf);
  });
  this.router.navigate(['/dashboard']);
}

searchForFuckSake() {
  this.restSourceService.getFormulaByOfNumber(this.informasiInputOf).subscribe((formula) => {
    this.selectedFormula = formula;
    this.restSourceService.getOrderQuantityByFormula(this.selectedFormula.productNo).subscribe((orderQuantityData) => {
      this.orderQuantityData = orderQuantityData;
      this.restSourceService.getRmQuantityByFormula(this.selectedFormula.productNo).subscribe((rmQuantityData) => {
        this.rmQuantityData = rmQuantityData;
      });
    });
  }, (error) => {
    this.errorMsg = 'Data tidak ada dalam database';
    return;
  });
}

logout() {
  var data = JSON.parse(localStorage.getItem('$2a$10$eKXxFtLgnQAoQYhtccIj5egtPHD7yFslrXeIDtMnyNxeMV.NzC51C'));
  this.userLogin.nik = atob(data['nik']);
  this.userLogin.login = data['login'];
  this.userLogin.logout = Date.now();
  this.restSourceService.postUserLogin(this.userLogin).subscribe((userLogin) => {
    console.log(this.userLogin);
  });
  this.authenticationService.signout();
  // this.authenticationService.logout();
}

}
