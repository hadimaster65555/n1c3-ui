import { Component, OnInit } from '@angular/core';
import { PotService } from '../service/pot.service';
import { BreakdownService } from '../service/breakdown.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { InformasiService } from '../service/informasi.service';
import { IdlePost } from '../model/IdlePost';

@Component({
  selector: 'app-waiting',
  templateUrl: './waiting.component.html',
  styleUrls: ['./waiting.component.css']
})
export class WaitingComponent implements OnInit {

  //Variabel Untuk RM, PACK dan WEIGH
  statusRM: number;
  statusPACK: number;
  statusWEIGH: number;
  jumlahRM: number;
  totalWeigh: number;

  btnEndOf: String;
  widthRM: number;
  widthWeigh: number;

  // Informasi Login
  nik: String;
  namaOperator: String;
  Shift: number;
  Formula: String;
  Of: Number;
  
  loginName: String;
  equipment: String;
  type: String;
  reason: String;
  start: Date;
  stop: Date;

  //Untuk Timer
  ticks = 0;
  minutesDisplay: number = 0;
  hoursDisplay: number = 0;
  secondsDisplay: number = 0;
  sub: Subscription;

  constructor(private potService: PotService,
              private breakdownService: BreakdownService,
              private informasiService: InformasiService) { }



  ngOnInit() {
    this.statusPACK = this.potService.statusPACK;
    this.statusRM = this.potService.statusRM;
    this.statusWEIGH = this.potService.statusWEIGH;
    this.jumlahRM = this.potService.jumlahRM;
    this.totalWeigh = this.potService.totalWeigh;
    this.widthRM = this.potService.widthRM;
    this.widthWeigh = this.potService.widthWeigh;

    this.informasiLoad();
    this.startTimer();
    this.loginName = this.potService.LoginName;
    this.equipment = this.potService.Equipment;
    this.type = this.potService.Type;
    this.reason = this.potService.Reason;
    this.start = this.potService.Start;
    this.stop = new Date();
    console.log("Dari POT Service : "+this.potService.NoLinePesee1);
    console.log("Dari POT Service : "+this.potService.Equipment);
    console.log("Dari POT Service : "+this.potService.LoginName);
    console.log("Dari POT Service : "+this.potService.Shift);
    console.log("Dari POT Service : "+this.potService.Tanggal);
    console.log("Dari POT Service : "+this.potService.Start);
    console.log("Dari POT Service : "+this.potService.Type);
    console.log("Dari POT Service : "+this.potService.Reason);
    console.log("Dari POT Service : "+this.potService.Status);


  }

  Submit(){
    let idlePost: IdlePost = new IdlePost(
    this.potService.NoLinePesee1,
    this.potService.Equipment,
    this.potService.Shift,
    this.potService.LoginName,
    this.potService.Tanggal,
    this.potService.ActionType,
    this.potService.Start,
    new Date(),
    this.potService.Reason,
    this.potService.Status
    );
    this.breakdownService.saveIdle(idlePost).subscribe();
    this.potService.Status = 'Idle';
    this.stopTimer();
  }

  informasiLoad() {
    if (this.informasiService.nama == null) {
      this.namaOperator = 'Mattasir';
    } else {
      this.namaOperator = this.informasiService.nama;
    }
    if (this.informasiService.nik == null) {
      this.nik = 'J69511';
    } else {
      this.nik = this.informasiService.nik;
    }
    if (this.informasiService.Formula == null) {
      this.Formula = 'ZZ92502583';
    } else {
      this.Formula = this.informasiService.Formula;
    }
    if (this.informasiService.OF == null) {
      this.Of = 400000076099;
    } else {
      this.Of = this.informasiService.OF;
    }
    if (this.informasiService.Shift == null) {
      this.Shift = 1;
    } else {
      this.Shift = this.informasiService.Shift;
    }
  }

  private stopTimer(){
    this.sub.unsubscribe();
    this.hoursDisplay = 0;
    this.minutesDisplay = 0;
    this.secondsDisplay = 0;
  }

  private startTimer() {
    this.hoursDisplay = 0;
    this.minutesDisplay = 0;
    this.secondsDisplay = 0;
    let timer = Observable.timer(1, 1000);
    this.sub = timer.subscribe(
          t => {
              this.ticks = t;

              this.secondsDisplay = this.getSeconds(this.ticks);
              this.minutesDisplay = this.getMinutes(this.ticks);
              this.hoursDisplay = this.getHours(this.ticks);
              console.log(this.secondsDisplay)
              this.stop = new Date();
              // value: String = `${new Date() | date: 'yyyy-MM-dd'}`;
            }
      );
  }

  //===============UNTUK TIMER=====================
  private getSeconds(ticks: number) {
    return this.pad(ticks % 60);
}

private getMinutes(ticks: number) {
     return this.pad((Math.floor(ticks / 60)) % 60);
}

private getHours(ticks: number) {
    return this.pad(Math.floor((ticks / 60) / 60));
}

private pad(digit: any) { 
    return digit <= 9 ? '0' + digit : digit;
}



}
